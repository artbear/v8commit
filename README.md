# **V8Commit** - инструмент для автоматического разборки\сборки обработок, отчетов и конфигураций для платформы 1С 8.3 в режиме управляемых форм. #

### Об инструменте ###

* Версия - 1.0.2
* [Консольная версия - 3.0.0a](https://github.com/pbazeliuk/V8Commit)

### Что к чему ###

* [Как мы управляем версиями](http://infostart.ru/public/261369/)
* [Как мы управляем версиями и тестированием 1C 8.3 (часть 2)](http://www.avtomat.biz/blog/kak-my-upravlyaem-versiyami-i-testirovaniem-1c-83-chast-2)
* [git-flow в 1С](http://www.avtomat.biz/blog/git-flow-v-1s)

### Как выйти на контакт ###

* Инструмент для проведения юнит-тестирования - [xUnitFor1C](https://github.com/xDrivenDevelopment/xUnitFor1C)
* Предложения и идеи [Базелюк Петр](mailto:pbazeliuk@gmail.com)
* Для неформального общения [G+](https://plus.google.com/u/0/+ПетрБазелюк)